-------------------------------------------------------------------------------
-- Title      : Phase Calculator
-- Project    : Calculation of phase difference between 2 clocks of same frequency 
-------------------------------------------------------------------------------
-- File       : top.vhd
-- Author     : Jubin MITRA
-- Contact	  : jubin.mitra@cern.ch
-- Company    : VECC, Kolkata, India
-- Created    : 23-02-2016
-- Last update: 23-02-2016
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- TOP FILE for running the code in CYCLONE IV E (	EP4CE22F17C6)						
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 23-02-2016  1.0      Jubin	Created
-------------------------------------------------------------------------------

--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;


--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--
entity top is
   port (

      
      CLK_REF_I   			                         : in  std_logic;
	  RESET_I  										 : in std_logic;
      TRIGGER_I                                      : in std_logic
      	  
   
   );
end entity;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--
architecture behavioral of top is

	--==================================== Constant Definition ====================================--   
	

	--==================================== Signal Definition ======================================--   
	signal PHASE_COUNTER_SELECT                     : std_logic_vector(2 downto 0) := (others => '0');
    signal PHASE_STEP                               : std_logic := '0';
    signal PHASE_UPDOWN                             : std_logic := '0';
    signal PHASE_DONE                               : std_logic := '0';
    signal TRIGGER_PULSE                            : std_logic := '0';
	
    signal REF_CLK									: std_logic := '0';
    signal REF_CLK_PHASE_SHIFTED					: std_logic := '0';
	
	signal ISSP_i                                   : std_logic_vector( 1 downto 0) := (others => '0');
	signal ISSP_o                                   : std_logic_vector(33 downto 0) := (others => '0');
    
	--=================================================================================================--
							--========####  Component Declaration  ####========-- 
	--=================================================================================================--   

    component test_design_phase_calc is
        port (
            clk_clk                                                               : in  std_logic                     := 'X'; -- clk
            phase_calculation_phase_shifted_clk_i_clk                             : in  std_logic                     := 'X'; -- clk
            phase_calculation_ref_clk_i_clk                                       : in  std_logic                     := 'X'; -- clk
            pll_dynamic_phase_shift_phase_counter_select_o_phase_counter_select_o : out std_logic_vector(2 downto 0);         -- phase_counter_select_o
            pll_dynamic_phase_shift_phase_done_i_phase_done_i                     : in  std_logic                     := 'X'; -- phase_done_i
            pll_dynamic_phase_shift_phase_step_o_phase_step_o                     : out std_logic;                            -- phase_step_o
            pll_dynamic_phase_shift_phase_updown_o_phase_updown_o                 : out std_logic;                            -- phase_updown_o
            pll_dynamic_phase_shift_trigger_pulse_i_trigger_pulse_o               : in  std_logic                     := 'X'; -- trigger_pulse_o
            reset_reset_n                                                         : in  std_logic                     := 'X'; -- reset_n
            phase_calculation_phase_value_integer_part_o_readdata                 : out std_logic_vector(31 downto 0);        -- readdata
            phase_calculation_phase_value_sign_o_writeresponsevalid_n             : out std_logic;                            -- writeresponsevalid_n
            phase_calculation_phase_calc_done_o_writeresponsevalid_n              : out std_logic                             -- writeresponsevalid_n
        );
    end component test_design_phase_calc;
	 
	component REFCLK_PLL IS
	PORT
	(
		areset		: IN STD_LOGIC  := '0';
		inclk0		: IN STD_LOGIC  := '0';
		phasecounterselect		: IN STD_LOGIC_VECTOR (2 DOWNTO 0) :=  (OTHERS => '0');
		phasestep		: IN STD_LOGIC  := '0';
		phaseupdown		: IN STD_LOGIC  := '0';
		scanclk		: IN STD_LOGIC  := '1';
		c0		: OUT STD_LOGIC ;
		c1		: OUT STD_LOGIC ;
		phasedone		: OUT STD_LOGIC 
	);
	end component;

    component issp is
		port (
			source : out std_logic_vector(1 downto 0);                     -- source
			probe  : in  std_logic_vector(33 downto 0) := (others => 'X')  -- probe
		);
	end component issp;
   --==============================================================================================--  

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

   --==================================== User Logic ==============================================--   

 u0 : component test_design_phase_calc
        port map (
            clk_clk                                                               => CLK_REF_I,
            phase_calculation_phase_shifted_clk_i_clk                             => REF_CLK_PHASE_SHIFTED,
            phase_calculation_ref_clk_i_clk                                       => REF_CLK,
            reset_reset_n                                                         => not ISSP_i(0),
            pll_dynamic_phase_shift_phase_done_i_phase_done_i                     => PHASE_DONE,      
            pll_dynamic_phase_shift_phase_updown_o_phase_updown_o                 => PHASE_UPDOWN,
            pll_dynamic_phase_shift_trigger_pulse_i_trigger_pulse_o               => ISSP_i(1),
            pll_dynamic_phase_shift_phase_step_o_phase_step_o                     => PHASE_STEP,
            pll_dynamic_phase_shift_phase_counter_select_o_phase_counter_select_o => PHASE_COUNTER_SELECT,
            phase_calculation_phase_value_integer_part_o_readdata                 => ISSP_o(31 downto 0),
            phase_calculation_phase_value_sign_o_writeresponsevalid_n             => ISSP_o(32),
            phase_calculation_phase_calc_done_o_writeresponsevalid_n              => ISSP_o(33)
            
        );


    pll_comp:
    REFCLK_PLL
	port map
	(
		areset		            => ISSP_i(0),
		inclk0		            => CLK_REF_I,
		phasecounterselect		=> PHASE_COUNTER_SELECT,
		phasestep	        	=> PHASE_STEP,
		phaseupdown		        => PHASE_UPDOWN,
		scanclk		            => CLK_REF_I,
		c0		                => REF_CLK,
		c1  	            	=> REF_CLK_PHASE_SHIFTED,
		phasedone	        	=> PHASE_DONE
	);
	
	issp_comp : 
    issp
	port map 
    (
		source => ISSP_i, 
		probe  => ISSP_o   
	);
        
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--