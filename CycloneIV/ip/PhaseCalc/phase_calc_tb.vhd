-------------------------------------------------------------------------------
-- Title      : Phase Calculation Using XOR Phase Detection Logic test bench
-- Project    : PHASE CALLIBRATION
-------------------------------------------------------------------------------
-- File       : phase_calc_tb.vhd
-- Author     : Jubin MITRA
-- Company    : 
-- Created    : 23-02-2016
-- Last update: 23-02-2016
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- input  clock
-- input phase shifted clock
-- sample clock			
-- output integer part 		
-- output fractional part
-- output decimal part			
------------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 27-07-2015  1.0      Jubin	Created
-------------------------------------------------------------------------------



--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;


--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity phase_calc_tb is

end phase_calc_tb;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture behavioral of phase_calc_tb is
 
--=================================================================================================--
						--========####  Component Declaration  ####========-- 
--=================================================================================================--   
component phase_calc is
   generic (
		constant NUMBER_OF_CYCLES			    : std_logic_vector( 31 downto 0) := X"0000_FFFF";
		constant CALIBRATION_FACTOR				: integer						 := 4000
   );
   port (
      RESET_I                                   : in  std_logic;
      REF_CLK_I                                 : in  std_logic; --  120 MHz
      PHASE_SHIFTED_CLK_I                       : in  std_logic; --  120 MHz + \theta
	  SAMPLE_CLK_I								: in  std_logic;
      PHASE_VALUE_INTEGER_PART_O                : out std_logic_vector( 31 downto 0);
      PHASE_VALUE_FRACTIONAL_PART_O             : out std_logic_vector( 31 downto 0);
	  PHASE_VALUE_SIGN_O						: out std_logic;
	  PHASE_CALC_DONE_O							: out std_logic
   );
end component;
	

--=================================================================================================--
						--========####  Signal Declaration  ####========-- 
--=================================================================================================--   
signal RESET									: std_logic;
signal REF_CLK									: std_logic;
signal PHASE_SHIFTED_CLK						: std_logic;
signal SAMPLE_CLK								: std_logic;
signal TRACE_CLK								: std_logic;
signal PHASE_VALUE_INTEGER_PART       	        : std_logic_vector( 31 downto 0);
signal PHASE_VALUE_FRACTIONAL_PART              : std_logic_vector( 31 downto 0);
signal PHASE_VALUE_SIGN 				 		: std_logic;
signal PHASE_CALC_DONE 					  		: std_logic;

signal TRACKER_COUNTER				            : std_logic_vector( 31 downto 0) := (others=>'0');
signal TRACKER_VALUE				            : std_logic_vector( 31 downto 0) := (others=>'0');
signal PAST_VALUE					            : std_logic;
signal XOR_SIGNAL					            : std_logic;


constant refclk_period	 						: time := 8.333   ns;			
constant phase_shift							: time := 260   ps;
constant sampleclk_period						: time := 19.5182886  ns;			
constant traceclk_period						: time := 10 ns; --ns
constant wait_period		 					: time := 10  ns;			

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--  
   
   --==================================== Port Mapping ======================================--

   u0: phase_calc
   generic map(
	  NUMBER_OF_CYCLES							=> X"0000_FFFF", --3E8
	  CALIBRATION_FACTOR						=> 8333
   )
   port map (
      RESET_I 		                            => RESET,
      REF_CLK_I		                            => REF_CLK,	  	  
      PHASE_SHIFTED_CLK_I	                    => PHASE_SHIFTED_CLK,
	  SAMPLE_CLK_I								=> SAMPLE_CLK,

      PHASE_VALUE_INTEGER_PART_O                => PHASE_VALUE_INTEGER_PART,
      PHASE_VALUE_FRACTIONAL_PART_O             => PHASE_VALUE_FRACTIONAL_PART,
	  PHASE_VALUE_SIGN_O						=> PHASE_VALUE_SIGN,
	  PHASE_CALC_DONE_O							=> PHASE_CALC_DONE
   );
   --==================================== Clock Generation =====================================--

   refclk_gen: process
   begin
		
		REF_CLK <= '1';
		wait for refclk_period/2;
		REF_CLK <= '0';
		wait for refclk_period/2;
   end process;

   phaseShiftClk_gen: process
   begin
		PHASE_SHIFTED_CLK <= '0';
		wait for phase_shift;
		loop
			PHASE_SHIFTED_CLK <= '1';
			wait for refclk_period/2;
			PHASE_SHIFTED_CLK <= '0';
			wait for refclk_period/2;
		end loop;
   end process;
   
   sampleclk_gen: process
   begin
			SAMPLE_CLK <= '1';
			wait for sampleclk_period/2;
			SAMPLE_CLK <= '0';
			wait for sampleclk_period/2;
   end process;

   traceclk_gen: process
   begin
			TRACE_CLK <= '1';
			wait for traceclk_period/2;
			TRACE_CLK <= '0';
			wait for traceclk_period/2;
   end process;
   --==================================== User Logic =====================================--
   XOR_SIGNAL <= REF_CLK xor PHASE_SHIFTED_CLK;
   tracker_proc:
   process(TRACE_CLK)
   begin
		if rising_edge(TRACE_CLK) then
			if  XOR_SIGNAL = '1' and PAST_VALUE = '0' then
				TRACKER_COUNTER <= x"0000_0000";
				PAST_VALUE <= XOR_SIGNAL;
			else
				TRACKER_COUNTER <= TRACKER_COUNTER + '1';
				PAST_VALUE <= XOR_SIGNAL;
			end if;
		end if;
   end process;
   
   tracker_value_proc:
   process(SAMPLE_CLK)
   begin
		if rising_edge(SAMPLE_CLK) then
			TRACKER_VALUE <= TRACKER_COUNTER;
		end if;
   end process;
   
	test_proc:
	process
	begin
		RESET <= '1';
		wait for wait_period;
		reset <= '0';
		wait for wait_period*10;
		wait;

	end process;
		
   --=====================================================================================--     
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--