onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/RESET_I
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/REF_CLK_I
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/PHASE_SHIFTED_CLK_I
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/SAMPLE_CLK_I
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/PHASE_VALUE_INTEGER_PART_O
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/PHASE_VALUE_FRACTIONAL_PART_O
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/PHASE_VALUE_SIGN_O
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/PHASE_CALC_DONE_O
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/csi_monitor_clk
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/avs_monitor_read
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/avs_monitor_readdata
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/avs_monitor_write
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/avs_monitor_writedata
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/avs_monitor_address
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/PHASE_VALUE_INTEGER_PART
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/PHASE_VALUE_FRACTIONAL_PART
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/PHASE_VALUE_SIGN
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/PHASE_CALC_DONE
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/PHASE_VALUE_INTEGER_PART_d1
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/PHASE_VALUE_FRACTIONAL_PART_d1
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/PHASE_VALUE_SIGN_d1
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/PHASE_CALC_DONE_d1
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/soft_reset
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/user_reset
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/reset
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/eoc_d31
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/avg_counter_1st_order
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/avg_total_1st_order
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/avg_counter_2nd_order
add wave -noupdate -radix hexadecimal /phase_calc_qsys_tb/u0/avg_total_2nd_order
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {22474472720 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 229
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {133489197056 ps}
