-------------------------------------------------------------------------------
-- Title      : Phase Calculation Using XOR Phase Detection Logic
-- Project    : PHASE CALLIBRATION
-------------------------------------------------------------------------------
-- File       : phase_calc.vhd
-- Author     : Jubin MITRA
-- Company    : 
-- Created    : 23-02-2016
-- Last update: 23-02-2016
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- input  clock
-- input phase shifted clock
-- sample clock			    (choice a prime value such that frequency multiplication is larger) ### frequency multiplication = phase shift step size###
-- output integer part 		
-- output fractional part
-- output decimal part									
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 23-02-2016  1.0      Jubin	Created
-------------------------------------------------------------------------------


--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity phase_calc_qsys_wrapper is 
  generic (
		constant NUMBER_OF_CYCLES			    : std_logic_vector( 31 downto 0) := X"0000_FFFF";
		-- ======================================================================================== --
		-- For calibration in degrees put 360 deg or time period T in ps unit like 8000 ps for 120 MHz
		-- ======================================================================================== --
		constant CALIBRATION_FACTOR				: integer						 := 4000
   );
   port (
  
      --================--
      -- Reset & Clocks --
      --================--    
      
      -- Reset:
      ---------
      
      RESET_I                                   : in  std_logic;
  
      -- Clocks:
      ----------
      
      REF_CLK_I                                 : in  std_logic; --  120 MHz
      PHASE_SHIFTED_CLK_I                       : in  std_logic; --  120 MHz + \theta
	  SAMPLE_CLK_I								: in  std_logic;
      
      --==============--
      -- PHASE VALUES --
      --==============--
      
      PHASE_VALUE_INTEGER_PART_O                : out std_logic_vector( 31 downto 0);
      PHASE_VALUE_FRACTIONAL_PART_O             : out std_logic_vector( 31 downto 0);
	  PHASE_VALUE_SIGN_O						: out std_logic;

	  PHASE_CALC_DONE_O							: out std_logic;
   	  
    --======================--
    --   AVALON MM Slave    --
    --======================-- 
        csi_monitor_clk                                                : in std_logic;
        avs_monitor_read                                               : in std_logic;
        avs_monitor_readdata                                           : out std_logic_vector(31 downto 0);
        avs_monitor_write                                              : in std_logic;
        avs_monitor_writedata                                          : in std_logic_vector(31 downto 0);
        avs_monitor_address                                            : in std_logic_vector( 1 downto 0)
    
);
end entity phase_calc_qsys_wrapper;


--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture mixed of phase_calc_qsys_wrapper is

	--==================================== Constant Definition ====================================--   
	

	--==================================== Signal Definition ======================================--   
    
    signal PHASE_VALUE_INTEGER_PART       	        : std_logic_vector( 31 downto 0):=(others=>'0');
    signal PHASE_VALUE_FRACTIONAL_PART              : std_logic_vector( 31 downto 0):=(others=>'0');
    signal PHASE_VALUE_SIGN 				 		: std_logic := '0';
    signal PHASE_CALC_DONE 					  		: std_logic := '0';

    
    signal PHASE_VALUE_INTEGER_PART_d1     	        : std_logic_vector( 31 downto 0):=(others=>'0');
    signal PHASE_VALUE_FRACTIONAL_PART_d1           : std_logic_vector( 31 downto 0):=(others=>'0');
    signal PHASE_VALUE_SIGN_d1				 		: std_logic := '0';
    signal PHASE_CALC_DONE_d1 				  		: std_logic := '0';
    
    signal soft_reset                               : std_logic := '0';
    signal user_reset                               : std_logic := '0';
    signal reset                                    : std_logic := '0';
 
    signal eoc_d31                                  : std_logic_vector ( 31 downto 0) := ( others => '0' );
    
    -- AVERAGING EFFECT
    signal avg_counter_1st_order                              : std_logic_vector ( 31 downto 0) := ( others => '0' );
    signal avg_total_1st_order                                : std_logic_vector ( 63 downto 0) := ( others => '0' );

    signal avg_counter_2nd_order                              : std_logic_vector ( 31 downto 0) := ( others => '0' );
    signal avg_total_2nd_order                                : std_logic_vector ( 63 downto 0) := ( others => '0' );    

    signal avg_counter_3rd_order                              : std_logic_vector ( 31 downto 0) := ( others => '0' );
    signal avg_total_3rd_order                                : std_logic_vector ( 63 downto 0) := ( others => '0' ); 
	--==================================== Component Declaration ====================================--          
    component phase_calc is
        generic (
            constant NUMBER_OF_CYCLES			    : std_logic_vector( 31 downto 0) := X"0000_FFFF";
            constant CALIBRATION_FACTOR				: integer						 := 4000
        );
        port (
          RESET_I                                   : in  std_logic;
          REF_CLK_I                                 : in  std_logic; 
          PHASE_SHIFTED_CLK_I                       : in  std_logic; 
          SAMPLE_CLK_I								: in  std_logic;
          PHASE_VALUE_INTEGER_PART_O                : out std_logic_vector( 31 downto 0);
          PHASE_VALUE_FRACTIONAL_PART_O             : out std_logic_vector( 31 downto 0);
          PHASE_VALUE_SIGN_O						: out std_logic;
          PHASE_CALC_DONE_O							: out std_logic
       );
    end component;
       

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

   --==================================== Port Mapping ==============================================--  

   u0: phase_calc
   generic map(
	  NUMBER_OF_CYCLES							=> NUMBER_OF_CYCLES,
	  CALIBRATION_FACTOR						=> CALIBRATION_FACTOR
   )
   port map (
      RESET_I 		                            => reset,
      REF_CLK_I		                            => REF_CLK_I,	  	  
      PHASE_SHIFTED_CLK_I	                    => PHASE_SHIFTED_CLK_I,
	  SAMPLE_CLK_I								=> SAMPLE_CLK_I,

      PHASE_VALUE_INTEGER_PART_O                => PHASE_VALUE_INTEGER_PART,
      PHASE_VALUE_FRACTIONAL_PART_O             => PHASE_VALUE_FRACTIONAL_PART,
	  PHASE_VALUE_SIGN_O						=> PHASE_VALUE_SIGN,
	  PHASE_CALC_DONE_O							=> PHASE_CALC_DONE
   );
   
   

   --==================================== User Logic ==============================================--  

   reset                               <= soft_reset or RESET_I or user_reset;

   --==================================== Avalon Bus Logic ==============================================--  
    avalon_read:
    process(RESET_I, csi_monitor_clk, avs_monitor_read)
    begin
        if RESET_I = '1' then
            avs_monitor_readdata <= (others=>'0');
        elsif rising_edge(csi_monitor_clk) then
                if avs_monitor_read = '1' and avs_monitor_address = "00" then
                    avs_monitor_readdata <= PHASE_VALUE_INTEGER_PART_d1; 
                elsif avs_monitor_read = '1' and avs_monitor_address = "01" then
                    avs_monitor_readdata <= PHASE_VALUE_FRACTIONAL_PART_d1; 
                elsif avs_monitor_read = '1' and avs_monitor_address = "10" then
                    avs_monitor_readdata <= x"0000_000" & "000" & PHASE_VALUE_SIGN_d1; 
                elsif avs_monitor_read = '1' and avs_monitor_address = "11" then
                    avs_monitor_readdata <= x"0000_000" & "000" & PHASE_CALC_DONE_d1;                
                end if;
        end if;
    end process;

    avalon_write:
    process(RESET_I, csi_monitor_clk, avs_monitor_write)
    begin
        if RESET_I = '1' then
            user_reset <= '0';
        elsif rising_edge(csi_monitor_clk) then
                if avs_monitor_write = '1' and avs_monitor_address = "00" then
                    user_reset          <= avs_monitor_writedata(0);                
                end if;
        end if;
    end process;

    --================================= Reset Automation and Value Load ====================================---

    reset_automation_and_value_load:
    process (RESET_I, csi_monitor_clk)
    begin
        if RESET_I = '1' then
            soft_reset                                            <= '0';
            avg_counter_1st_order                                           <= (others => '0');
            avg_total_1st_order                                             <= (others => '0');
            avg_counter_2nd_order                                           <= (others => '0');
            avg_total_2nd_order                                             <= (others => '0');
            
        elsif rising_edge(csi_monitor_clk) then
                eoc_d31                                           <= eoc_d31(30 downto 0) & PHASE_CALC_DONE;
                PHASE_CALC_DONE_d1                                <= PHASE_CALC_DONE;

            if PHASE_CALC_DONE_d1 = '0' and PHASE_CALC_DONE = '1' then
                    avg_counter_1st_order                                   <= avg_counter_1st_order + '1';        
                    avg_total_1st_order                                     <= avg_total_1st_order   + PHASE_VALUE_INTEGER_PART;
                    
            end if;
            if avg_counter_1st_order = x"0000_0010" then
                    avg_counter_1st_order                                   <= (others => '0');
                    avg_total_1st_order                                     <= (others => '0');
                    
                    avg_counter_2nd_order                                   <= avg_counter_2nd_order + '1';
                    avg_total_2nd_order                                     <= avg_total_2nd_order + avg_total_1st_order(35 downto 4);
            end if;
            if avg_counter_2nd_order = x"0000_0010" then
                    avg_counter_2nd_order                                   <= (others => '0');
                    avg_total_2nd_order                                     <= (others => '0');
                    
                    avg_counter_3rd_order                                   <= avg_counter_3rd_order + '1';
                    avg_total_3rd_order                                     <= avg_total_3rd_order + avg_total_2nd_order(35 downto 4);
            end if;
            if avg_counter_3rd_order = x"0000_0010" then                    
                    avg_counter_3rd_order                                   <= (others => '0');
                    avg_total_3rd_order                                     <= (others => '0');
                    --PHASE_VALUE_INTEGER_PART_d1                   <= PHASE_VALUE_INTEGER_PART;
                    PHASE_VALUE_INTEGER_PART_d1                             <= avg_total_3rd_order(35 downto 4);
                    PHASE_VALUE_FRACTIONAL_PART_d1                          <= PHASE_VALUE_FRACTIONAL_PART;
                    PHASE_VALUE_SIGN_d1                                     <= PHASE_VALUE_SIGN;
                   
                    PHASE_CALC_DONE_O                                       <= PHASE_CALC_DONE_d1;
            end if;

            if eoc_d31(31) = '1' and eoc_d31(30) = '1' then
                soft_reset                  <= '1';
            else
                soft_reset                  <= '0';                
            end if;
            
        end if;
    end process;
                    PHASE_VALUE_INTEGER_PART_O                   <= PHASE_VALUE_INTEGER_PART_d1;
                    PHASE_VALUE_FRACTIONAL_PART_O                <= PHASE_VALUE_FRACTIONAL_PART_d1;
                    PHASE_VALUE_SIGN_O                           <= PHASE_VALUE_SIGN_d1;


end architecture mixed;