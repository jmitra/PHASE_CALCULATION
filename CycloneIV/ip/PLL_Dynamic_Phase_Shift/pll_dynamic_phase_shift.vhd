-------------------------------------------------------------------------------
-- Title      : Dynamic Phase Shift for IOPLL
-- Project    : PHASE CALLIBRATION
-------------------------------------------------------------------------------
-- File       : pll_dynamic_phase_shift.vhd
-- Author     : Jubin MITRA
-- Company    : VECC
-- Created    : 26-02-2016
-- Last update: 26-02-2016
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- User asserts a shift pulse and it shifts accordingly to the set value
-- Shifting is done in rising edge of the pulse		
-------------------------------------------------------------------------------
-- Reference
-- https://www.altera.com/en_US/pdfs/literature/ug/ug_altpll.pdf
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 26-02-2016  1.0      Jubin	Created
-------------------------------------------------------------------------------



--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity pll_dynamic_phase_shift is
   port (
  
      --================--
      -- Reset & Clocks --
      --================--    
      
      -- Reset:
      ---------
      
      RESET_I                                   : in  std_logic;
  
      -- Clocks:
      ----------
      
      SCANCLK_I                                 : in  std_logic; --  120 MHz
      
      --======================--
      -- DYNAMIC PHASE SHIFTS --
      --======================--
      
      PHASE_COUNTER_SELECT_O              		: out std_logic_vector (2 DOWNTO 0);
      PHASE_STEP_O                              : out std_logic;
      PHASE_UPDOWN_O                            : out std_logic;
      PHASE_DONE_I                              : in  std_logic;
		
      TRIGGER_PULSE_I                           : in  std_logic;        --- PULSE have to be high one clock cycle of scanclk

    --======================--
    --   AVALON MM Slave    --
    --======================-- 
        csi_monitor_clk                                                : in std_logic;
        avs_monitor_read                                               : in std_logic;
        avs_monitor_readdata                                           : out std_logic_vector(31 downto 0);
        avs_monitor_write                                              : in std_logic;
        avs_monitor_writedata                                          : in std_logic_vector(31 downto 0);
        avs_monitor_address                                            : in std_logic_vector( 1 downto 0)
   
   );
end pll_dynamic_phase_shift;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture behavioral of pll_dynamic_phase_shift is
	
	  
   --==================================== Signal Definition =====================================--   
	signal  no_of_phase_shifts                                         : std_logic_vector(31 downto 0):=x"0000_0001";	
	signal  phase_shifts_counter                                       : std_logic_vector(31 downto 0):=x"0000_0000";	
	signal  phase_counter_select                                       : std_logic_vector( 2 downto 0):="010";	
    signal  phase_shift_directions                                     : std_logic := '1';
    
    signal  trigger_pulse                                              : std_logic := '0';
    signal  user_trigger_pulse                                         : std_logic := '0';
    signal  trigger_pulse_detect                                       : std_logic_vector( 3 downto 0) := x"0";
    signal  isTriggerAsserted                                          : std_logic := '0';
    
    type state is (IDLE, ASSERTED, ASSERTED_HOLD, DEASSERTED, PHASE_DONE, HOLD);
    signal dynamic_phase_shift_state : state;

		--=====================================================================================--  

--=================================================================================================--
begin                 --========####   Architecture Body   ####========--- 
--=================================================================================================--  
   
   --==================================== Avalon Bus Logic ==============================================--  
    avalon_read:
    process(RESET_I, csi_monitor_clk, avs_monitor_read)
    begin
        if RESET_I = '1' then
            avs_monitor_readdata <= (others=>'0');
        elsif rising_edge(csi_monitor_clk) then
                if avs_monitor_read = '1' and avs_monitor_address = "00" then
                    avs_monitor_readdata <= x"0000_000" & "000" & PHASE_DONE_I; 
                elsif avs_monitor_read = '1' and avs_monitor_address = "01" then
                    avs_monitor_readdata <= x"0000_000" & "0" & phase_counter_select; 
                elsif avs_monitor_read = '1' and avs_monitor_address = "10" then
                    avs_monitor_readdata <= no_of_phase_shifts; 
                elsif avs_monitor_read = '1' and avs_monitor_address = "11" then
                    avs_monitor_readdata <= x"0000_0000";                
                end if;
        end if;
    end process;

    avalon_write:
    process(RESET_I, csi_monitor_clk, avs_monitor_write)
    begin
        if RESET_I = '1' then
            phase_counter_select                <= "010";
            no_of_phase_shifts                  <= x"0000_0001";
            phase_shift_directions              <= '1';
        elsif rising_edge(csi_monitor_clk) then
                if avs_monitor_write = '1' and avs_monitor_address = "00" then
                    phase_shift_directions      <= avs_monitor_writedata(0);
                elsif avs_monitor_write = '1' and avs_monitor_address = "01" then
                    phase_counter_select        <= avs_monitor_writedata(2 downto 0);
                elsif avs_monitor_write = '1' and avs_monitor_address = "10" then
                    no_of_phase_shifts          <= avs_monitor_writedata;
                elsif avs_monitor_write = '1' and avs_monitor_address = "11" then
                    user_trigger_pulse          <= avs_monitor_writedata(0);
                end if;
        end if;
    end process;
   
   --==================================== User Logic =============================================--   

       trigger_pulse                            <= user_trigger_pulse or TRIGGER_PULSE_I;
       
   --============================================================================================--
   --================================# TRIGGER PULSE DETECT #====================================--
   --============================================================================================--
    trigger_proc:
    process(RESET_I,SCANCLK_I)
    begin
        if RESET_I = '1' then
            isTriggerAsserted                   <= '0';
            trigger_pulse_detect                <= x"0";
        elsif rising_edge(SCANCLK_I) then
            trigger_pulse_detect                <= trigger_pulse_detect(2 downto 0) & trigger_pulse;
            if trigger_pulse_detect(3) = '0' and trigger_pulse_detect(2) = '1' then
                isTriggerAsserted               <= '1';
            else
                isTriggerAsserted               <= '0';
            end if;
        end if;
    end process;
  
   --============================================================================================--
   --================================# DYNAMIC PHASE SHIFT FSM #=================================--
   --============================================================================================--
   -- STATES: IDLE, ASSERTED, DEASSERTED, PHASE_DONE, HOLD
    dynamic_phase_shift_proc:
    process(RESET_I,SCANCLK_I)
    begin
        if RESET_I = '1' then
            PHASE_STEP_O                                <= '0';
            dynamic_phase_shift_state                   <= IDLE;
            phase_shifts_counter                        <= (others => '0');
        elsif falling_edge(SCANCLK_I) then
            case dynamic_phase_shift_state is
                when IDLE =>
                    if isTriggerAsserted = '1' then
                        dynamic_phase_shift_state       <= ASSERTED;
                        phase_shifts_counter            <= no_of_phase_shifts;
                        PHASE_COUNTER_SELECT_O          <= "000";
                        PHASE_UPDOWN_O                  <= '0';
                    end if;
                when ASSERTED =>
                    if PHASE_DONE_I = '0' then
                        dynamic_phase_shift_state       <= ASSERTED;
                    else 
                        PHASE_STEP_O                    <= '1';
                        dynamic_phase_shift_state       <= ASSERTED_HOLD;
                    end if;
                when ASSERTED_HOLD =>
                        dynamic_phase_shift_state       <= DEASSERTED;
                        PHASE_COUNTER_SELECT_O          <= phase_counter_select;
                        PHASE_UPDOWN_O                  <= phase_shift_directions;
                when DEASSERTED =>
                        if PHASE_DONE_I = '0' then
                            PHASE_STEP_O                  <= '0';
                            dynamic_phase_shift_state   <= PHASE_DONE;
                        end if;
                when PHASE_DONE => 
                    phase_shifts_counter                 <= phase_shifts_counter - '1';
                    dynamic_phase_shift_state            <= HOLD;
                when HOLD =>
                
                    PHASE_COUNTER_SELECT_O               <= "000";
                    PHASE_UPDOWN_O                       <= '0';
                   if phase_shifts_counter > x"0000_0000" then
                    dynamic_phase_shift_state           <= ASSERTED;
                   else
                    dynamic_phase_shift_state           <= IDLE;
                   end if;
            end case;
        end if;
    end process;
   
   

end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--