-------------------------------------------------------------------------------
-- Title      : Dynamic Phase Shift for IOPLL
-- Project    : PHASE CALLIBRATION
-------------------------------------------------------------------------------
-- File       : pll_dynamic_phase_shift.vhd
-- Author     : Jubin MITRA
-- Company    : VECC
-- Created    : 26-02-2016
-- Last update: 26-02-2016
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- User asserts a shift pulse and it shifts accordingly to the set value
-- Shifting is done in rising edge of the pulse		
-------------------------------------------------------------------------------
-- Reference
-- https://www.altera.com/en_US/pdfs/literature/ug/ug_altpll.pdf
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 26-02-2016  1.0      Jubin	Created
-------------------------------------------------------------------------------



--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;


--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity phase_shift_tb is

end phase_shift_tb;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture behavioral of phase_shift_tb is
 
--=================================================================================================--
						--========####  Component Declaration  ####========-- 
--=================================================================================================--   
component pll is
	port
	(
		areset		            : in  STD_LOGIC  := '0';
		inclk0		            : in  STD_LOGIC  := '0';
		phasecounterselect		: in  STD_LOGIC_VECTOR (2 downto 0) :=  (OTHERS => '0');
		phasestep	        	: in  STD_LOGIC  := '0';
		phaseupdown		        : in  STD_LOGIC  := '0';
		scanclk		            : in  STD_LOGIC  := '1';
		c0		                : out STD_LOGIC ;
		locked	            	: out STD_LOGIC ;
		phasedone	        	: out STD_LOGIC 
	);
end component;
	
component pll_dynamic_phase_shift is
   port (
  
      RESET_I                                   : in  std_logic;     
      SCANCLK_I                                 : in  std_logic; --  120 MHz
      
      --======================--
      -- DYNAMIC PHASE SHIFTS --
      --======================--
      
      PHASE_COUNTER_SELECT_O              		: out std_logic_vector (2 DOWNTO 0);
      PHASE_STEP_O                              : out std_logic;
      PHASE_UPDOWN_O                            : out std_logic;
      PHASE_DONE_I                              : in  std_logic;
		
      TRIGGER_PULSE_I                           : in  std_logic;        --- PULSE have to be high one clock cycle of scanclk

    --======================--
    --   AVALON MM Slave    --
    --======================-- 
        csi_monitor_clk                                                : in std_logic;
        avs_monitor_read                                               : in std_logic;
        avs_monitor_readdata                                           : out std_logic_vector(31 downto 0);
        avs_monitor_write                                              : in std_logic;
        avs_monitor_writedata                                          : in std_logic_vector(31 downto 0);
        avs_monitor_address                                            : in std_logic_vector( 1 downto 0)
   
   );
end component;
--=================================================================================================--
						--========####  Signal Declaration  ####========-- 
--=================================================================================================--   
signal RESET									: std_logic := '0';
signal REF_CLK									: std_logic := '0';
signal SCAN_CLK	    							: std_logic := '0';
signal REF_CLK_PHASE_SHIFTED					: std_logic := '0';
signal LOCKED               					: std_logic := '0';


signal PHASE_COUNTER_SELECT                     : std_logic_vector(2 downto 0) := (others => '0');
signal PHASE_STEP                               : std_logic := '0';
signal PHASE_UPDOWN                             : std_logic := '0';
signal PHASE_DONE                               : std_logic := '0';
signal TRIGGER_PULSE                            : std_logic := '0';

signal avs_monitor_read                         : std_logic;
signal avs_monitor_write                        : std_logic;
signal avs_monitor_readdata                     : std_logic_vector(31 downto 0) := (others => '0');
signal avs_monitor_writedata                    : std_logic_vector(31 downto 0) := (others => '0');
signal avs_monitor_address                      : std_logic_vector( 1 downto 0) := (others => '0');



constant refclk_period	 						: time := 8.333   ns;			
constant scanclk_period				    		: time := 19.5182886  ns;			
constant wait_period		 					: time := 10  ns;			

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--  
   
   --==================================== Port Mapping ======================================--
    pll_comp:
    pll
	port map
	(
		areset		            => RESET,
		inclk0		            => REF_CLK,
		phasecounterselect		=> PHASE_COUNTER_SELECT,
		phasestep	        	=> PHASE_STEP,
		phaseupdown		        => PHASE_UPDOWN,
		scanclk		            => SCAN_CLK,
		c0		                => REF_CLK_PHASE_SHIFTED,
		locked	            	=> LOCKED,
		phasedone	        	=> PHASE_DONE
	);
    
    pll_dynamic_phase_shift_comp:
    pll_dynamic_phase_shift 
    port map (
  
      RESET_I                                   => RESET,    
      SCANCLK_I                                 => SCAN_CLK,
      PHASE_COUNTER_SELECT_O              		=> PHASE_COUNTER_SELECT,
      PHASE_STEP_O                              => PHASE_STEP,
      PHASE_UPDOWN_O                            => PHASE_UPDOWN,
      PHASE_DONE_I                              => PHASE_DONE,
		
      TRIGGER_PULSE_I                           => TRIGGER_PULSE,

        csi_monitor_clk                         => SCAN_CLK,
        avs_monitor_read                        => avs_monitor_read,
        avs_monitor_readdata                    => avs_monitor_readdata,
        avs_monitor_write                       => avs_monitor_write,
        avs_monitor_writedata                   => avs_monitor_writedata,
        avs_monitor_address                     => avs_monitor_address
   
   );
    
   --==================================== Clock Generation =====================================--

   refclk_gen: process
   begin
		
		REF_CLK <= '1';
		wait for refclk_period/2;
		REF_CLK <= '0';
		wait for refclk_period/2;
   end process;


   
   scanclk_gen: process
   begin
			SCAN_CLK <= '1';
			wait for scanclk_period/2;
			SCAN_CLK <= '0';
			wait for scanclk_period/2;
   end process;


   --==================================== User Logic =====================================--

   
	test_proc:
	process
	begin
		RESET <= '1';
		wait for wait_period;
		reset <= '0';
		wait for wait_period*10;
        TRIGGER_PULSE <= '1';
		wait for wait_period*10;
        TRIGGER_PULSE <= '0';        
		wait;

	end process;
		
   --=====================================================================================--     
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--