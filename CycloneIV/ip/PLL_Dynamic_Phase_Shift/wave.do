onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider PLL
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/areset
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/inclk0
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/phasecounterselect
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/phasestep
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/phaseupdown
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/scanclk
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/c0
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/locked
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/phasedone
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/sub_wire0
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/sub_wire1
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/sub_wire2_bv
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/sub_wire2
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/sub_wire3
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/sub_wire4
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/sub_wire5
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_comp/sub_wire6
add wave -noupdate -divider {PLL PHASE SHIFT COMPONENT}
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/RESET_I
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/SCANCLK_I
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/PHASE_COUNTER_SELECT_O
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/PHASE_STEP_O
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/PHASE_UPDOWN_O
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/PHASE_DONE_I
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/TRIGGER_PULSE_I
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/csi_monitor_clk
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/avs_monitor_read
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/avs_monitor_readdata
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/avs_monitor_write
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/avs_monitor_writedata
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/avs_monitor_address
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/no_of_phase_shifts
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/phase_shifts_counter
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/phase_counter_select
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/phase_shift_directions
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/trigger_pulse
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/user_trigger_pulse
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/trigger_pulse_detect
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/isTriggerAsserted
add wave -noupdate -radix hexadecimal /phase_shift_tb/pll_dynamic_phase_shift_comp/dynamic_phase_shift_state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1270630 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {27622061 ps} {109633351 ps}
