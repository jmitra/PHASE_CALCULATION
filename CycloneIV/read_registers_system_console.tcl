# Written By Jubin Mitra
# Dated: 23 - 02 - 2016
# Excute in system console :  N

set master_service_path [lindex [get_service_paths master] 0]
open_service master $master_service_path

proc hex2dec {largeHex} {
    set res 0
    foreach hexDigit [split $largeHex {}] {
        set new 0x$hexDigit
        set res [expr {16*$res + $new}]
    }
    return $res
}

set phase_sign_code [lindex [master_read_32 $master_service_path [expr 0x30+(0x02<<2)] 0x1] 0]
set phase_sign_hex [format "%0x" $phase_sign_code]
set phase_sign_num [hex2dec $phase_sign_hex]
puts "Phase Sign = $phase_sign_num"

set phase_code [lindex [master_read_32 $master_service_path [expr 0x30+(0x00<<2)] 0x1] 0]
set phase_hex [format "%0x" $phase_code]
set phase_num [hex2dec $phase_hex]
 puts "Phase Value (Integer) = [expr $phase_num]"
set path_delay 0
if "$phase_sign_num == 0" {puts "Phase Value (Integer) = [expr ((7.217e-10)*($phase_num*$phase_num*$phase_num))+((-7.517e-06)*($phase_num*$phase_num))+((0.9896)*($phase_num))+75.64]"}
if "$phase_sign_num == 1" {puts "Phase Value (Integer) = [expr ((0.9468*$phase_num)+5.207)]"}

set phase_frac_code [lindex [master_read_32 $master_service_path [expr 0x30+(0x01<<2)] 0x1] 0]
set phase_frac_hex [format "%0x" $phase_frac_code]
set phase_frac_num [hex2dec $phase_frac_hex]
puts "Phase Value (Fractional) = $phase_frac_num"


set phase_calcdone_code [lindex [master_read_32 $master_service_path [expr 0x30+(0x03<<2)] 0x1] 0]
set phase_calcdone_hex [format "%0x" $phase_calcdone_code]
set phase_calcdone_num [hex2dec $phase_calcdone_hex]
puts "Phase Calculation Done = $phase_calcdone_num"

